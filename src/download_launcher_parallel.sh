# This script is intended to launch in parallel the downloading of ENCODE ChIP-seq data sets.
#
# Dependencies:
#--------------
# download_one_dataset.sh
#
# Note: that it assumes the dependency script is located in the same folder as this script
#
#usage: bash download_launcher_parallel.sh /path/to/thefolder/containing/experiment/metadata/ /path/to/the/file/containing/list/of/datasets/to/download /path/to/the/output/repository 

#parse arguments
in_dir=;
if [ -z "$1" ]
    then 
	echo "No input file folder specified. Exiting..."; 
	exit 1
    else	
	in_dir=$1
fi


datasets=;
if [ -z "$2" ]
  then
       echo "No list of data sets provided. Exiting..";
  else
       datasets=$2
fi

curr_dir=`pwd`
repository=;
if [ -z "$3" ]
  then
       echo "No repository specified to copy the files to. Will use current folder \"./repository\"";
       repository=$curr_dir/repository
  else
       repository=$3
fi

#print arguments
echo "------------------------------------------------------"
echo "Input folder: $in_dir"
echo "Dataset list: $datasets"
echo "Download repository: $repository"
echo "------------------------------------------------------"

PATH_TO_DOWNLOAD_ONE_DATASET_SCRIPT="./download_one_dataset.sh"

mkdir -p $repository
i=0

cd $in_dir
while IFS='' read -r line || [[ -n "$line" ]]
    do

    ((i++))

    #limit the number of processes
    # if [ "$i" -lt "70" ] 
    #    then 
    #      continue
    # fi 
   
    #if [ "$i" -gt "70" ]
    #  then
    #    break
    #fi

    #get dataset name
    d="$(find . | grep -i "$line" | head -1)"
    
    #check if the folder name is not empty
    if [[ -z "${d// }" ]]
       then
         continue
    fi

    echo "Processing folder $d"
    
    time bash $PATH_TO_DOWNLOAD_ONE_DATASET_SCRIPT $in_dir$(basename $d) $repository &
  	
done < "$datasets"

