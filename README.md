# ChIP-eat

A complete pipeline designed to process ChIP-seq data sets for TF binding site and binding profile prediction. Starting from FASTQ files to peak calling, sequence scoring, enrichment analyis with data set dependent threshold and centrality p-value calculation, and TFBS visualization. The scripts provided in this pipeline are intended to reduce as much as possible the user input needed during individual data transformations steps while analyzing ChIP-seq data sets for transcription factor binding site and binding profile prediction. ChIP-seq datasets can be downloaded via the [ENCODE](https://www.encodeproject.org/) project or from [GEO](https://www.ncbi.nlm.nih.gov/geo/).

## Getting Started

To be able to use ChIP-eat, you need to download the project on your machine and follow the steps below to install and set up the needed paths to the software dependencies.

### Prerequisites

A UNIX based operating system is needed in order to run the ChIP-eat pipeline or having Bash Unix Shell support on a Microsoft Windows machine. 
**Note** that the shell should support the `gzip`, `tr`, `awk` and `sed` commands. If this is not the case, install them using your package manager 

#### Peak calling tools

You can install [JAMM](https://github.com/mahmoudibrahim/JAMM) and/or [MACS](https://github.com/taoliu/MACS/wiki/Install-macs2). We recommend to install both of them, as one can outperform the other depending on the quality/resolution of the ChIP-seq data set. The best result can be chosen based on visual interpretation and the centrality p-value that is assigned to each set of results.**Note** that this pipeline was developed and tested using JAMM v1.0.7.5 and MACS v2.1.1.

#### R statistical environment

You can install the latest version of the R statistical environment from [here](https://cran.r-project.org/bin/). No additional R package is required for this pipeline.

#### Software suites

In order to perform all the data extraction and transformation steps, several dedicated sets of tools are required:

Install **BOWTIE 2** from [here](https://sourceforge.net/projects/bowtie-bio/files/bowtie2/2.3.0/)
Make sure you create also an index file with BOWTIE for the genome version of interest. This can be achieved by following [this](http://bowtie-bio.sourceforge.net/bowtie2/manual.shtml#getting-started-with-bowtie-2-lambda-phage-example) tutorial.

**Important** the same genome version should be used throughout the ChIP-eat pipeline!

Install **SAMTOOLS** from [here](http://www.htslib.org/)

Install **BEDTOOLS 2** from [here](https://github.com/arq5x/bedtools2)

Besides the genome index file created by BOWTIE, you will need a file containing the **chromosome sizes** and a FASTA file for the **reference genome**. As an example, for Homo Sapiens, these files can be retrieved from [here](http://hgdownload.cse.ucsc.edu/goldenPath/hg38/bigZips/)

### Installing and setting up ChIP-eat

There is no need for an actual installation of ChIP-eat. Downloading all the project and maintaining its folder structure should suffice. The only settings needed are the paths to the above mentioned prerequisites. 
These paths have to be set as follows:

In the *alignment_pipeline.sh* script found in the *bin* folder of the project:
```
PATH_TO_BOWTIE2="/indicate/the/path/to/your/local/installation/of/bowtie2"
PATH_TO_SAMTOOLS="indicate/the/path/to/your/local/installation/of/samtools"
PATH_TO_BEDTOOLS2="indicate/the/path/to/your/local/installation/of/bedtools2"
```

In the *results_processing.sh* script found in the *bin* folder of the project:
```
PATH_TO_BEDTOOLS2="indicate/the/path/to/your/local/installation/of/bedtools2"
```

### Running the ChIP-eat pipeline

The first part of the processing pipeline would be the data alignment and peak calling. This can be achieved as follows:
Download one or several ChIP-seq data sets from either the [ENCODE](https://www.encodeproject.org/) project or from [GEO](https://www.ncbi.nlm.nih.gov/geo/). For reliable results, make sure that the datasets contain also *control* files bsides the *treatment* or *input* files. It is assumed that the data sets consist of **gzipped FASTQ** files for both *control* and *treatment*. 
Example of an input file name:
```
ENCFF000QQJ.fastq.gz
```

#### 1. Alignment and peak calling

Once you have obtained the ChIP-seq data sets, run the alignment part of the pipeline:

```
cd /path/to/ChIP-eat/bin
```
```
bash alignment_pipeline.sh /path/to/my/data/set(s) /path/to/bowtie2/index/file/
```
The folder structure is maintained for the output.
Run the peak calling part:

```
bash jamm_peak_calling.sh /path/to/JAMM/software/launching/script /path/to/aligned/input/data/sets /path/to/the/chromosome/sizes/file
```
or 

```
bash macs_peak_calling.sh /path/to/macs2/binary /path/to/aligned/input/data/sets
```

The results of the peak calling will be output in a *results* folder of the project, so make sure the writing permissions are assigned to the project folder.

**Notes** the BOWTIE index file should be created following the instructions in the **Prerequisites** part of this file.
          this part is time consuming if using JAMM as the peak calling software. In average, it takes around 9 hours to process a data set with two control files and two treatment files due to the large amount of peaks it finds. MACS will take in average around 10 minutes to process the same data set.  

More information about the individual steps in this part of the pipeline and the parameter settings used can be found in the workflow diagram *alignment_and_peak_calling_diagram.pdf* located in the *doc* folder of the project.

#### 2. Resulting peaks processing

Once the peak calling is performed, the results can be processed by

```
bash results_processing.sh JAMM /path/to/JAMM/processed/data/sets /path/to/chromosome/sizes /path/to/reference/genome/fasta/file <window_size>
```
or 

```
bash results_processing.sh MACS /path/to/MACS/processed/data/sets /path/to/chromosome/sizes /path/to/reference/genome/fasta/file <window_size>
```

where **<window_size>** is the amount of base pairs (region) to be increased around the peak summit upstream and downstream. This region will be analyzed for potential binding sites for the transcription factor. For instance, a decent value for this parameter would be 500. This means that 500 bp will be added upstream and 500 bp will be added downstream.

More information about the individual steps in this part of the pipeline and the parameter settings used can be found in the workflow diagram *results_processing_diagram.pdf* located in the *doc* folder of the project.

#### 3. Result visualization

This last step of the pipeline allows the user to have a visual interpretation of the results. This can be achieved by

```
 bash results_plotting.sh /path/to/processed/results
```

This will create a pair of plots for each data set and they will be output in a *plots* subfolder of the *results*.

**Note** that if a data set was processed with both JAMM and MACS, the plots will contain the results from both peak calling tools in the same page allowing the comparison.

### Example with test data

**Setting the paths**

```
cd /user/ChIP-eat/bin
vim alignment_pipeline.sh
```
```
<search and set>

PATH_TO_BOWTIE2="/user/tools/bowtie2"
PATH_TO_SAMTOOLS="/user/tools/samtools"
PATH_TO_BEDTOOLS2="/user/tools/bedtools2"

<save and exit>
```
```
vim results_processing.sh

<search and set>

PATH_TO_BOWTIE2="/user/bowtie2"

<save and exit>
```
**Pipeline**

Three example data sets are provided in the data folder. For the sake of time and space saving, these contain the results after the alignment and peak calling part of the pipeline. The commands that were used in the first part of the pipeline are the following:

```
bash alignment_pipeline.sh  /user/ChIP-eat/data /user/bowtie2/index/HG38_no_alt_analysis_set.fna.bowtie_index
```
```
bash jamm_peak_calling.sh /user/tools/JAMM-JAMMv1.0.7.5/JAMM.sh  /user/ChIP-eat/data /user/tools/hg38_chromosome_sizes
```
```
bash macs_peak_calling.sh /user/tools/macs2/bin/macs2 /user/ChIP-eat/data  
```

The following steps can be applied on the test data sets provided:

```
bash results_processing.sh  JAMM /user/ChIP-eat/data/JAMM /user/tools/hg38_chromosome_sizes /user/tools/hg38_whole_genome_fasta.fa 500 
```
```
bash results_processing.sh  MACS /user/ChIP-eat/data/MACS /user/tools/hg38_chromosome_sizes /user/tools/hg38_whole_genome_fasta.fa 500 
```
```
bash results_plotting.sh /user/ChIP-eat/results/processed/
```

## Authors

* **Anthony Mathelier** - *Design and tutoring* - [amathelier](https://github.com/amathelier) 
* **Marius Gheorghe** - *Development and design* - [MariusGh](https://bitbucket.org/MariusGh)

## License

This project is licensed under the GNU General Public License 

## Acknowledgments

* Hat tip to anyone who's code was used

