#!/bin/bash

# This script will process each of the data sets present in the input folder and 
# make use of the MACS2 peak calling software. It assumes that there is at least 1
# data set in the input folder provided as the first argument and that the data 
# set(s) contain(s) both replicates and control files. The control files are 
# identified by having the word "control" in their name (case insensitive). 
# These data sets should be the resulting transformed data processed by the 
# "alignment_pipeline.sh" script.
#
# More details about each step and the used parameters can be found in the diagram 
# "peak_calling_diagram.pdf" located in the doc folder.
#
# Dependencies: 
# -------------
# gunzip
# MACS2 (v2.1.1)
#
# Parameters:
# -----------
# arg[1]: the path to the MACS2 tool and its macs binary
# arg[2]: the input folder containing data set(s) processed through alignment_pipeline.sh 
#
# usage: bash macs_peak_calling.sh /path/to/macs2/binary /path/to/input/data/sets 


#parse arguments
macs_path=;
if [ -z "$1" ]
    then
        echo "No path to MACS specified. Exiting...";
        exit 1
    else
        macs_path=$1
fi

in_dir=;
if [ -z "$2" ] 
    then
	echo "No input folder specified. Exiting...";
	exit 1
    else
	in_dir=$2	
fi	

#create delimited string from array
function join_by { local IFS="$1"; shift; echo "$*"; }

#print working paths
echo "-----------------------------------------"
echo "Input folder: $in_dir";
echo "MACS: $macs_path";
echo "-----------------------------------------"

#parameter declaration
workspace=$(dirname $in_dir)/results
i=1
nb_folders="$(ls -l $in_dir | grep -c ^d)"

#main loop over input folders
cd $in_dir
for d in */
   do

    echo "Started processing folder $i out of $nb_folders: ${d%/*} at `date`";
    ((i++))

    out_dir=$workspace/MACS/$d
    
    if [ -e $out_dir ]
       then
         echo " Folder $out_dir already exists. Skipping..";
         continue
    fi
    
    #create folder structure and copy files locally
    mkdir -p $out_dir
    mkdir $out_dir/controls
    mkdir $out_dir/replicates
    
    #get controls and replicates separately	
    for bed in $(find ./$d -name "*.bed.gz")
    do
        if [[ "$(echo "$bed" | tr '[:upper:]' '[:lower:]')" == *"control"* ]]
      	   then
             cp $bed $out_dir/controls
           else
	     cp $bed $out_dir/replicates
   	fi
    done
	
    log=$out_dir/${d%/*}.log
#   echo "log file: $log"
       
    #check for controls and unzip them or log "no controls"
    cd $out_dir/controls
    controls=(*)
    if [ ${#controls[@]} -eq 0 ]
        then 
	   echo "No control files present\n" >> $log
	else	
  	   gunzip *.gz
	   controls=(*)
	   #prefix with the absolute path
	   controls=("${controls[@]/#/$out_dir/controls/}")
    fi

    #check for replicates and unzip them or log "no replicates"	
    cd $out_dir/replicates
    replicates=(*)
    if [ ${#replicates[@]} -eq 0 ]
        then
	   echo "No replicates present\n" >> $log  		
	else
           gunzip *.gz;
	   replicates=(*)
	   #prefix with the absolute path	
	   replicates=("${replicates[@]/#/$out_dir/replicates/}") 
    fi	

    #create control and replicate by concatenating all control files and all replicate files separately
    controls=`join_by " " "${controls[@]}"`
    control_file=$out_dir/controls/${d%/*}_control.bed
    cat $controls > $control_file; 
    replicates=`join_by " " "${replicates[@]}"`
    replicate_file=$out_dir/replicates/${d%/*}_replicate.bed
    cat $replicates > $replicate_file
    
    #launch MACS if there are controls AND replicates
    if [[ ! -z $controls ]] && [[ ! -z $replicates ]] 
	then
           cd $out_dir
           $macs_path callpeak -t $replicate_file -c $control_file -f BED -g hs -n ${d%/*} >> $log 2>&1
	   #clean up
           rm -R $out_dir/controls
           rm -R $out_dir/replicates
       else
	   echo "Not all needed input data present. Skipping folder $d \n" >> $log;
   fi
	
    cd $in_dir

done        

