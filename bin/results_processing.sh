#!/bin/bash

# This script will process the narrowPeak files obtained with either JAMM or MACS
# peak calling software in order to identify the top scoring TFBSs for a certain TF.
# It assumes that there is at least 1 data set in the input folder provided as the
# second argument and that the data set(s) contain(s) a file with the narrowPeak
# extension (formatted accordingly). These data sets should represent the output of
# JAMM or MACS. Note that for the JAMM type of results, the filtered narrowPeak file
# will be processed. More details about the workflow and parameter settings can be
# found in the "results_processing_diagram.pdf" located in the doc folder.
#
# The threshold calculation script used in this process represent an adaptation of 
# the TFBS visualization tools created by Rebecca Worsley Hunt:
# https://github.com/wassermanlab/TFBS_Visualization
#
# Dependencies: 
# -------------
# awk
# bedtools
# R version > 3.0
#
# Other scripts called in this script:
# ------------------------------------
# centrimo_pval
# pwm_searchPFF
# compute_thresholds.R
# get_top_hits.R


###### !!!! MODIFY ACCORDINGLY THE PATH(S) TO DEPENDENCIES #######
PATH_TO_BEDTOOLS2=

##################################################################
 
# Parameters:
# -----------
# arg[1]: the input data type (either JAMM or MACS)
# arg[2]: the input folder containing the data sets processed with JAMM or MACS
# arg[3]: the chromosome size file (version recommended hg38). This file can be obtained from
# http://hgdownload.cse.ucsc.edu/goldenpath/hg38/bigZips/hg38.chrom.sizes
# arg[4]: the reference genome FASTA file (version recommended hg38). This file can be obtained from
# http://hgdownload.cse.ucsc.edu/goldenPath/hg38/bigZips/hg38.fa.gz
# arg[5]: the size of the window. Note that this amount of BP will be added upstream and downstream from peakMax
# arg[6]: the folder that contains all the PWMs created based on the PFMs extracted from JASPAR. Commented for now. 
# arg[7]: the path to the file containing the mapping between the TFs and the JASPAR profile. Commented for now. Defaulted
#
# usage: bash results_processing.sh JAMM /path/to/JAMM/processed/data/sets /path/to/chromosome/sizes /path/to/reference/genome/fasta 500 

#parse arguments
data_type=;
if [ -z "$1" ]
    then
        echo "No data type specified. Exiting...";
        exit 1
    else
        data_type=$1
fi

in_dir=
if [ -z "$2" ]
    then
        echo "No input file folder specified. Exiting...";
        exit 1
    else
        in_dir=$2
fi

hg_chr_file=;
if [ -z "$3" ]
    then
        echo "No chromosome size file specified. Exiting...";
        exit 1
    else
        hg_chr_file=$3
fi

hg_gen_file=;
if [ -z "$4" ]
    then
        echo "No reference genome FASTA file specified. Exiting...";
        exit 1
    else
        hg_gen_file=$4
fi

window_size=;
if [ -z "$5" ]
    then
        echo "No window size specified. Using 500bp";
        window_size=500
    else
        window_size=$5
fi

pwm_folder="../../src/PWM"
pwm_folder=;
if [ -z "$6" ]
    then
        echo "No PWM folder specified. Using: ../../src/PWM";
        pwm_folder="../../src/PWM"
    else
        pwm_folder=$6
fi


########## PATHS TO THE SCRIPTS CALLED ####################
#
# !!Note that it is assumed the scripts are
# in the src folder of the project.
#
result_folder="../../results/processed"
# faulty data sets
no_peaks_folder="$result_folder/no_peaks"
no_pwm_folder="$result_folder/no_pwm"
no_pvalue_folder="$result_folder/no_pval"
# script locations
seq_scoring_code="../../src/pwm_searchPFF"
centrimo_pval_code="../../src/centrimo_pval"
compute_thresholds="../../src/compute_thresholds.R"
get_top_hits="../../src/get_top_hits.R"

############ END OF RELATIVE PATH DEFINITION ##############

#print script arguments
echo "------------------------------------------------------"
echo "Data type: $data_type"
echo "Window size: $window_size"
echo "Input folder: $in_dir"
echo "PWM folder: $pwm_folder"
echo "Results folder: $result_folder"
echo "------------------------------------------------------"

mkdir -p $result_folder/$(basename $in_dir)

#counters
no_peaks=0
no_pval=0
no_pwm=0

cd $in_dir
i=1
nb_folders="$(ls -l $in_dir | grep -c ^d)"

#main loop over input folders
for d in */
   do
   
   echo "Processing folder $i out of $nb_folders"
   ((i++))

   #retrieve TF name from folder name
   IN=$d
   folderIN=(${IN//_/ })
   tf_name=${folderIN[-1]%/*} #get last element which is the TF name

   #search for the PWM file for this TF (sorted in reverse to get the latest version)
   pwms="$(ls $pwm_folder | grep -i ${tf_name}.pwm | sort -r)"
   set -- $pwms 

   #reset previous pwm
   pwm=""

   #check if any pwm was found for this TF 
   if [[ -z "${pwms// }" ]]
      then
      echo "No PWM file matching the transcription factor $tf_name was found. Skipping..";
      move_folder=$no_pwm_folder/$(basename $in_dir)
      mkdir -p $move_folder 
      cp -r $d $move_folder
      ((no_pwm++))
      continue
   else
      for p in $pwms
         do
           #split the file name and get the ID and the TF name
           IN=$p
           pwmIN=(${IN//_/ })
           #id=${pwmIN[0]}
           tf=${pwmIN[1]%.*}
           
           #check for an exact match 
           if [[ "${tf,,}" == "${tf_name,,}" ]]
              then
                pwm=$p
                break
           fi
       done
       if [[ ! -z $pwm ]]
          then
             echo "The PWM used for folder ${d#/*} with the transcription factor $tf_name is: $pwm";
          else
             continue
       fi
   fi #end if pwms were found
   
     
   #create folder structure and output file name
   out_dir=$result_folder/$(basename $in_dir)/${d%/*}/$data_type
   
   #check if the folder already exists and skip it
   if [ -d "$out_dir" ]
        then
	echo "Folder $out_dir already exists in results. Skipping...";
        continue
   fi
     	
   mkdir -p $out_dir
   out_file=$out_dir/${data_type}_${tf_name}_peaks
 
   #check if JAMM had input in this folder
   if [ $data_type = "JAMM" ]
      then
      #echo "I'm in JAMM";
          if [ -d "$d/peaks" ]
            then
               peaks_file=$d/peaks/filtered.peaks.narrowPeak
               if [ -e $peaks_file ]
		 then
            	     # 1. Add offset to chrStart
                     awk -v OFS='\t' '{print $1,$2+$10,$2+$10+1,$4,$5,$6}' $peaks_file > $out_file
                     # hg_chr_file=$hg38_chr
                     # hg_gen_file=$hg38_gen
                 else
                     echo "Folder $d does not contain a filtered peaks file. Check the JAMM log file for more details.";
                     move_folder=$no_peaks_folder/$data_type
                     mkdir -p $move_folder
                     cp -r $d $move_folder
                     ((no_peaks++))  
                     continue               		
               fi
            else 
              echo "Folder $d does not contain the \"peaks\" subfolder. Check the JAMM log file for more details.";
	      move_folder=$no_peaks_folder/$data_type
              mkdir -p $move_folder
              cp -r $d $move_folder
              ((no_peaks++))
              continue
          fi
  elif [ $data_type = "MACS" ]
     then
       # echo "I'm in MACS";
        peaks_file=$d/${d%/*}_peaks.narrowPeak
        if [ -e $peaks_file ]
           then
              # 1. Add offset to chrStart
              awk -v OFS='\t' '{print $1,$2+$10,$2+$10+1,$4,$5,$6}' $peaks_file > $out_file
              #hg_chr_file=$hg38_chr
              #hg_gen_file=$hg38_gen 
           else
              echo "Folder $d does not contain a results file. Check the MACS log file for more details.";
              move_folder=$no_peaks_folder/$data_type
              mkdir -p $move_folder
              cp -r $d $move_folder
              ((no_peaks++))
              continue
        fi
  else
     echo "No supported data_type passed as argument. Only JAMM, ZERONE or MACS supported.";
     exit 1
  fi

      ######### 2. Increase area around peak max with 'window_size' upstream and downstream
      $PATH_TO_BEDTOOLS2/bedtools slop -i $out_file -g $hg_chr_file -b $window_size > ${out_file}_${window_size}bp
      out_file=${out_file}_${window_size}bp
      
      ######### 3. Map back to the genome and get a FASTA file
      $PATH_TO_BEDTOOLS2/fastaFromBed -fo ${out_file}.fa -fi $hg_gen_file -bed $out_file
      out_file=${out_file}.fa

      ######### 4. Scoring the sequences and output
      $seq_scoring_code $pwm_folder/$pwm $out_file 0.0 -b -n $tf_name > ${out_file}.score
      out_file=${out_file}.score
      # Output format:
      # 1. Chromosome:start-end (end-start is SLOP (window_size*2)+1
      # 2. Description
      # 3. TF name
      # 4. Strand
      # 5. Absolute score
      # 6. Relative score
      # 7. Start offset of the TFBS in the sequence from the window start (Note it is one based, so no need to +1)
      # 8. End of TFBS relative to start offset (8th column - 7th column gives the TFBS sequence length)
      # 9. Sequence (Note: that it is 1 based, so there is no need to +1 the start offset of the TFBS 

      ######### 5. Format file for centrimo and visualization tools 
      awk -v OFS='\t' '{print $1,$4,$9,$3,(($8-$7)+1),(($7+$8)/2)-'$window_size',$5,$6,$7,$8}' $out_file | awk -v OFS='\t' -F":" '$1=$1' | awk -v OFS='\t' '{sub(/\-/,"\t",$2)};1' > ${out_file}.ext
      # Output format:
      # 1. Chromosome
      # 2. Start of the TFBS on chromosome (chrStart from step 4 + column 7 from step 4)
      # 3. End of the TFBS on chromosome (resulting chrStart + length of TFBS (8th column - 7th column from step 4)
      # 4. Strand
      # 5. Sequence
      # 6. TF name
      # 7. TFBS sequence length (Note that +1 because it is zero based coming out of the C code)
      # 8. Distance to peak max (!!Note: that this is relative to the chrStart and chrEnd info coming from SLOP)
      # 9. Absolute score
      # 10. Relative score
      # 11. Start offset of the TFBS relative to window start
      # 12. End of the TFBS relative to window start

      ######### 6. Run the R script to dynamically calculate score thresholds and get the number of top hits and total sequences
      # Parameters:
      #  1. Path to script wrapper for threshold calculation
      #  2. Input file containing the scored sequences (formatted file from step 5)
      #  3. Type of score (relative or absolute) - same as score.type parameter in compute_threshold.R
      #  4. The column holding the distance to the peak summit - same as distance.column parameter in compute_threshold.R
      #  5. The column containing the score (should correspond to the score.type chosen) - same as score.column parameter
      #  6. The name of the output file - same as output.file.name parameter in compute_threshold.R

      centrimo_input=($(Rscript $get_top_hits $compute_thresholds ${out_file}.ext "relative" 8 10 ${out_file}.top))
      
      ######### 7. Run the centrimo p-value calculation code
      #
      # Parameters:
      # 1. The path to the file containing the top scoring sequences (output of the compute_threshold.R script) and formatted as in step 5
      # 2. The number of sequences being above the score threshold a.k.a nb_hits - (centrimo_input[1]) - top sequences
      # 3. The total number of sequences for this TF (total number of lines in formatted file from step 5)
      # 4. The size of the window used in this pipeline
      # 5. The type of p-value calculation (ratio or discrete). Refer to the centrimo_pval.cpp for more information
      #
      # Note: if the output file with the top hits has 0 size or first argument of centrimo is 0 then the threshold
      # were not successfully calculated or signal was too poor/indecisive
      if [ ! -e ${out_file}.top ] || [ ! -s ${out_file}.top ] || [[ -z "${centrimo_input[1]// }" ]]
         then
            echo "No motif threshold calculated for $d. Maybe signal is too poor. No centrimo for you :/"
            move_folder=$no_pvalue_folder/$d/$data_type
            mkdir -p $move_folder
            mv $out_dir/* $move_folder
            rm -r $out_dir
            ((no_pval++))
            continue
         else
            $centrimo_pval_code ${out_file}.top "${centrimo_input[1]}" "${centrimo_input[2]}" $window_size "ratio" > ${out_file}.pval
      fi

done
echo "-------------------------------------------------------"
echo "Batch: $(basename $in_dir)"
echo "For data type $data_type there were:"
echo "$no_pwm data sets with no ${pwm_folder#/*}"
echo "$no_peaks data sets with no peaks found during processing"
echo "$no_pval data sets with no automatic threshold calculated due to poor signal"
echo "-------------------------------------------------------"
