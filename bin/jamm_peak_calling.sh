#!/bin/bash

# This script will process each of the data sets present in the input folder and 
# make use of the JAMM peak calling software. It assumes that there is at least 1
# data set in the input folder provided as the first argument and that the data 
# set(s) contain(s) both replicates and control files. The control files are 
# identified by having the word "control" in their name (case insensitive). 
# These data sets should be the resulting transformed data processed by the 
# "alignment_pipeline.sh" script.
#
# More details about each step and the used parameters can be found in the diagram 
# "peak_calling_diagram.pdf" located in the doc folder.
#
# Dependencies: 
# -------------
# gunzip
# JAMM v1.0.7.5
#
# Parameters:
# -----------
# arg[1]: the path to the peak calling script of JAMM
# arg[2]: the input folder containing data set(s) processed through alignment_pipeline.sh 
# arg[3]: the chromosome size file (version recommended hg38). This file can be obtained from
# http://hgdownload.cse.ucsc.edu/goldenpath/hg38/bigZips/hg38.chrom.sizes
#
# usage: bash jamm_peak_calling.sh /path/to/JAMM/peak/calling/script /path/to/input/data/sets /path/to/the/chromosome/sizes/file 


#parse arguments
jamm_path=;
if [ -z "$1" ]
    then
        echo "No path to JAMM specified. Exiting...";
        exit 1
    else
        jamm_path=$1
fi

in_dir=;
if [ -z "$2" ]
    then 
	echo "No input file folder specified. Exiting..."; 
	exit 1
    else	
	in_dir=$2
fi

chr_sizes=;
if [ -z "$3" ]
    then
	echo "No chromosome size file supplied. Exiting...";
	exit 1                                   
    else
	chr_sizes=$3
fi

#print arguments
echo "------------------------------------------------------"
echo "Input folder: $in_dir";
echo "JAMM: $jamm_path";
echo "Chromosome size file: $chr_sizes";
echo "------------------------------------------------------";

#parameter declaration
workspace=$(dirname $in_dir)/results
i=1
nb_folders="$(ls -l $in_dir | grep -c ^d)"

#main loop over input folders
cd $in_dir
for d in */ 
   do
    echo "Started processing folder $i out of $nb_folders: ${d%/*} at `date`";
    ((i++))
 
    out_dir=$workspace/JAMM/$d

    #create folder structure and copy files locally
    if [ ! -d "$out_dir" ] 
	then	
	    mkdir -p $out_dir
	    mkdir $out_dir/controls
	    mkdir $out_dir/replicates     
    
	    #get controls and replicates separately	
	    for bed in $(find ./$d -name "*.bed.gz")
	    do
                if [[ "$(echo "$bed" | tr '[:upper:]' '[:lower:]')" == *"control"* ]]
		    then 
		  	cp $bed $out_dir/controls
		    else
        	        cp $bed $out_dir/replicates
		fi		
	    done

	    log=$out_dir/${d%/*}.log
    
	    #check for controls and unzip them or log "no controls"
	    all_ok=true
	    if [ -n "$(ls -A $out_dir/controls)" ]
	 	then
  		    cd $out_dir/controls
	            gunzip *.gz
		else
		    echo "No control files found\n" >> $log;
		    all_ok=false	
	    fi

	    #check for replicates and unzip them or log "no replicates"
	    if [ -n "$(ls -A $out_dir/replicates)" ]
		then		
		    cd $out_dir/replicates
		    gunzip *.gz
		else
		    echo "No replicates found\n" >> $log;
		    all_ok=false	
	    fi	 	
		
  	  #launch JAMM
	    if $all_ok
		then  	
		   bash $jamm_path -s $out_dir/replicates -c $out_dir/controls -g $chr_sizes -o $out_dir -e "auto" -T $out_dir/tmp >> $log 2>&1; 
		else
		    echo "Not all needed input files present." >> $log;
  	 fi

	   #clean up
	   rm -R $out_dir/controls
	   rm -R $out_dir/replicates

	   cd $in_dir	

      else
	echo "Folder $d already exists in results. Skipping..."
      fi	
done
