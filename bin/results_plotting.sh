#!/bin/bash

# This script will plot the TFBS found for a certain TF. It will process the files
# that are the output from the oftware in order to identify the top scoring TFBSs
# for a certain TF. It assumes that there is at least 1 data set in the input folder
# provided as the only argument and that the data set(s) represent the output of the 
# results_processing.sh script. More details about the workflow and parameter settings
# can be found in the "results_processing_diagram.pdf" located in the doc folder.
# The plotting functions used in this process represent an adaptation of the TFBS
# visualization tools created by Rebecca Worsley Hunt:
# https://github.com/wassermanlab/TFBS_Visualization
#
# Dependencies: 
# -------------
# R version > 3.0
#
# Other scripts called within this script
# --------------------------------------- 
# call_plotting.R
#
# Parameter:
# ----------- 
# arg[1]: the input folder containing the results processed with the results_processing.sh script
#
# usage: bash results_plotting.sh /path/to/processed/results 

#parse arguments
in_dir=;
if [ -z "$1" ]
    then
        echo "No input file folder specified. Exiting..."
        exit 1
    else
        in_dir=$1
fi

######### RELATIVE PATH TO SCRIPT CALLED ##########
#
# !!Note that it is assumed the script is located in
# the src folder of the project.
#
wrapper_plotting="../../src/call_plotting.R"
###################################################

i=1
nb_folders="$(ls -l $in_dir | grep -c ^d)"

cd $in_dir
for d in */
   do
 
      echo "Processing folder $i out of $nb_folders: ${d%/*}"
      ((i++))

      if [ -z "$(find $d -maxdepth 2 -mindepth 1)" ]
        then
          echo "Folder empty. Skipping.."
          continue
      fi

     #get the needed files for the plotting functions
     # 1. find all files with extension score.ext
     # 2. remove all leading "./"
     # 3. replace the "\n" character that the find function appends at each result with " "
     # 4. prepend the absolute path to each found file
     data_files="$(find ./$d -name "*.score.ext" | sort -z | sed "s|^\./||" | tr '\n' '#' | sed -e "s:$d:$in_dir$d:g")"
     
#     echo "Data files"
#     echo "$data_files"
#     echo "------------------------------------------";
    
     #check if there are data files or skip folder
     if [ ${#data_files[@]} -eq 0 ]
        then
           echo "No data files found in folder $d. Skipping...."
           continue
     fi
     
     #get the corresponding files containing the thresholds and pvalues from centrimo
     threshold_files="$(find ./$d -name "*.score.ext.thresholds.rds" | sort -z | sed "s|^\./||" | tr '\n' '#' | sed -e "s:$d:$in_dir$d:g")"

#     echo "Threshold files"
#     echo "$threshold_files"
#     echo "------------------------------------------";

     pvalue_files="$(find ./$d -name "*.score.pval" | sort -z | sed "s|^\./||" | tr '\n' '#' | sed -e "s:$d:$in_dir$d:g")"
     if [[ -z $pvalue_files ]]
        then
           pvalue_files="NA"
     fi

      #call the wrapper of the plotting script
      Rscript $wrapper_plotting $in_dir $data_files $threshold_files $pvalue_files ${d%/}

      echo "------------------------------------------"
  
done 
