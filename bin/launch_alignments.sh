for fastq in $(find . -name "*.fastq.gz")
do
    if [ -e $fastq ]
    then
        dir=$(dirname $fastq);
        name=$(basename $fastq .fastq.gz);
        sbatch --job-name=alg.$name --output=$dir/$name.log \
            ../../bin/alignment.sbatch $fastq $dir;
    fi;
done;
