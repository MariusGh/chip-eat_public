#!/bin/bash

# Will loop through all data sets in the input folder (passed as first argument) and
# call the alignment pipeline for each of the FASTQ files present in each subfolder
# of each data set. If a list of data sets is provided as the second argument, then
# it will retrieve the path to each of these data sets, assuming they are located in
#  the input folder passed as first argument and call the alignment pipeline for each. 
#
# Dependencies:
# -------------
# alignment_pipeline.sh
#
# usage: bash alignment_launcher_parallel.sh /path/to/alignment/script/ /path/to/input/folder/ [/path/to/dataset/list]

#parse arguments
alignment_script=;
if [ -z "$1" ]
    then 
	echo "No alignment script specified. Exiting..."; 
	exit 1
    else	
	alignment_script=$1
fi

in_dir=;
if [ -z "$2" ]
    then 
	echo "No input file folder specified. Exiting..."; 
	exit 1
    else	
	in_dir=$2
fi


datasets=;
if [ -z "$3" ]
  then 
       echo "No list of data sets provided. Will process all from the input folder";
  else       
       datasets=$3
fi

#path to the index file for the reads alignment
bowtie_ind="/lsc/bowtie2ind/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.bowtie_index"

#counters for the data sets
i=0
nb_folders=;
if [ -z "$3" ]
  then
    nb_folders="$(ls -l $in_dir | grep -c ^d)"
  else
    nb_folders=$(wc -l < $datasets)
fi

#print parameter settings
echo "---------------------------------------"
echo "Input folder: $in_dir"
echo "Alignment script: $alignment_script"
echo "Bowtie index file: $bowtie_ind"
echo "Data sets file: $datasets"
echo "Folders to process: $nb_folders"
echo "---------------------------------------"

# check if a list of data sets was passed as argument
cd $in_dir
if [ -z "$datasets" ]
  # no list specified, taking all data sets in input folder
  then
     for d in */ 
        do
          ((i++))

            #limit the number of processes
            #if [ "$i" -lt "70" ]
            #   then
            #       continue
            #fi

            if [ "$i" -gt "70" ]
              then
                break
            fi

#           echo "Dataset $i out of $nb_folders: $d" 
            time bash $alignment_script $in_dir/$d $bowtie_ind &     
      done
  # specified list of data sets
  else 
      while IFS='' read -r line || [[ -n "$line" ]]
         do
           ((i++))
           d="$(find . | grep -i "$line" | head -1)"
           d=$(basename $d)
#           echo "Dataset $i out of $nb_folders: $d"
            time bash $alignment_script $in_dir/$d $bowtie_ind & 
      done < "$datasets"
fi
