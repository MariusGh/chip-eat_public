#usage: bash jamm__narrow_auto_launcher.sh /storage/scratch/anthoma/ENCODE_TF_ChIPseq/results/20160719_alignments_1-41/ /storage/scratch/marius/ENCODE/data/hg38_chromosome_sizes 

#parse arguments
in_dir=;
if [ -z "$1" ]
    then 
	echo "No input file folder specified. Exiting..."; 
	exit 1
    else	
	in_dir=$1
fi


datasets=;
if [ -z "$2" ]
  then
       echo "No list of data sets provided. Exiting..";
  else
       datasets=$2
fi

curr_dir=`pwd`
repository=;
if [ -z "$3" ]
  then
       echo "No repository specified to copy the files to. Will use current folder \"./repository\"";
       repository=$curr_dir/repository
  else
       repository=$3
fi

#print arguments
echo "------------------------------------------------------"
echo "Input folder: $in_dir"
echo "Dataset list: $datasets"
echo "Download repository: $repository"
echo "------------------------------------------------------"

mkdir -p $repository
i=0
cd $in_dir
while IFS='' read -r line || [[ -n "$line" ]]
    do

    ((i++))

    #limit the number of processes
    # if [ "$i" -lt "70" ] 
    #    then 
    #      continue
    # fi 
   
    #if [ "$i" -gt "70" ]
    #  then
    #    break
    #fi

    #get dataset name
    d="$(find . | grep -i "$line" | head -1)"
    
    #check if the folder name is not empty
    if [[ -z "${d// }" ]]
       then
         continue
    fi

    echo "Processing folder $d"
    
    time bash /storage/scratch/marius/ENCODE/results/paired_ended/download_one_dataset.sh $in_dir$(basename $d) $repository &
  	
done < "$datasets"

