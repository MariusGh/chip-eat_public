# This script is intended to download ENCODE ChIP-seq data sets that contain paired-ended FASTQ files.
# It expects a list of data set names to download. It will retrieve information about
# the sequences contained in the FASTQ files (single-ended or paired-ended). If pair-ended, it will retrieve
# information about the paired file and their order (1 or 2) based on the metadata.json file that should be
# present for each experiment or control file. It will download all the pair-ended files to be later
# distributed to their corresponding data set. It will also create a summary table for all the data sets 
# that are in the list provided as input, regarding each FASTQ file.

# usage bash download_one_dataset.sh /storage/mathelierarea/raw/ENCODE/TF_ChIPseq_raw/Experiments/ /storage/scratch/marius/ENCODE/results/paired_ended/repository


#parameter declaration
file_type="replicate"

run_type_tag=".run_type"
PAIR_ENDED_LABEL="\"paired-ended\""

# name of the pair file
paired_with_tag=".paired_with"
# order of the file (1 or 2)
paired_end_tag=".paired_end"

# file types
CONTROL_LABEL="control"
REPLICATE_LABEL="replicate"

BASE_URL='https://www.encodeproject.org'


#arguments
d=$1
repository=$2

#echo "Folder $d"
#echo "Repository $repository"

cd $d
d=$(basename $d)           

mkdir -p $repository/$d
log=$repository/$d/${d}_download_log.txt

# loop over FASTQ files in each dataset
for fastq in $(find . -name "*.fastq.gz")
    do
      if [ -e $fastq ]
          then
             #get metadata file
             json=$(find $(dirname "${fastq}") -name "metadata.json")
             # echo "JSON file is: $json"
                          
              run_type=$(cat $json | jq $run_type_tag )
              
               #check if control file
               if [[ "$(echo "$fastq" | tr '[:upper:]' '[:lower:]')" == *"control"* ]]
                   then 
                      file_type=$CONTROL_LABEL
                      download_dir=$repository/$d/controls
                   else
                      file_type=$REPLICATE_LABEL
                      download_dir=$repository/$d/replicates
               fi

               #  echo "Run type for $fastq is $run_type"
               if [ "$run_type" == "$PAIR_ENDED_LABEL" ]
                  then
                     # get pair and order
                     paired_with=$(cat $json | jq $paired_with_tag)
                     paired_with=$(echo "$paired_with" | tr -d '"')
                     paired_end=$(cat $json | jq $paired_end_tag)

                     # create folder structure and download files
                     mkdir -p $download_dir     
                     wget -c -o $log "$BASE_URL/files/$(basename ${fastq%.fastq.gz})/@@download/$(basename $fastq)" -P $download_dir
		     wget -c -o $log "$BASE_URL$paired_with@@download/$(basename $paired_with).fastq.gz" -P $download_dir 

#		     echo "Paired: $BASE_URL/files/$(basename ${fastq%.fastq.gz})/@@download/$(basename $fastq)"
#		     echo "With: $BASE_URL$paired_with@@download/$(basename $paired_with).fastq.gz"

                     # rename so we keep track of the order in the file name
		     file_order="1"
                     pair_order="2"
                     if [ "$(echo "$paired_end" | tr -d '"')" == "2" ]
                         then
                            file_order="2"
                            pair_order="1"
                     fi
                     mv $download_dir/$(basename $fastq) $download_dir/$(basename ${fastq%.fastq.gz})_${file_order}_$(basename $paired_with).fastq.gz
                     mv $download_dir/$(basename $paired_with).fastq.gz $download_dir/$(basename ${paired_with%.fastq.gz})_${pair_order}_$(basename ${fastq%.fastq.gz}).fastq.gz

#        	      echo "File: $(basename ${fastq%.fastq.gz})_$file_order.fastq.gz"	 
#                     echo "Pair: $(basename ${paired_with%.fastq.gz})_$pair_order.fastq.gz"
		else
                    #reset labels for single-ended
		    paired_with="NA"
 		    paired_end="NA"
                    wget -c -o $log "$BASE_URL/files/$(basename ${fastq%.fastq.gz})/@@download/$(basename $fastq)" -P $download_dir
#                   echo "Single: $BASE_URL/files/$(basename ${fastq%.fastq.gz})/@@download/$(basename $fastq)"
	        fi
       fi
done

