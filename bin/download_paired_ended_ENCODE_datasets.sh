# This script is intended to download ENCODE ChIP-seq data sets that contain paired-ended FASTQ files.
# It expects a list of data set names to download. It will retrieve information about
# the sequences contained in the FASTQ files (single-ended or paired-ended). If pair-ended, it will retrieve
# information about the paired file and their order (1 or 2) based on the metadata.json file that should be
# present for each experiment or control file. It will download all the pair-ended files to be later
# distributed to their corresponding data set. It will also create a summary table for all the data sets 
# that are in the list provided as input, regarding each FASTQ file.

# usage bash get_json_info_pair_ended.sh /storage/mathelierarea/raw/ENCODE/TF_ChIPseq_raw/Experiments/ /storage/scratch/marius/ENCODE/results/list_of_datasets.tsv


in_dir=;
if [ -z "$1" ]
    then
        echo "No input folder specified. Exiting...";
        exit 1
    else
        in_dir=$1
fi


datasets=;
if [ -z "$2" ]
  then
       echo "No list of data sets provided. Exiting.." #Will process all from the input folder";
      # TODO make it to loop over the return of 'ls'
      # datasets=($(ls -d $in_dir))
  else
       datasets=$2
fi


curr_dir=`pwd`
repository=;
if [ -z "$3" ]
  then
       echo "No repository specified to copy the files to. Will use current folder \"./repository\"";
       repository=$curr_dir/repository
  else
       repository=$3
fi


#parameter declaration
file_type="replicate"

run_type_tag=".run_type"
PAIR_ENDED_LABEL="\"paired-ended\""

# name of the pair file
paired_with_tag=".paired_with"
# order of the file (1 or 2)
paired_end_tag=".paired_end"

# file types
CONTROL_LABEL="control"
REPLICATE_LABEL="replicate"

BASE_URL='https://www.encodeproject.org'

RESULTS=()
#add header
#RESULTS+=$(printf "dataset\tfile\ttype\trun_type\tpaired_with\tpaired_end")
RESULTS+=("dataset\tfile\ttype\trun_type\tpaired_with\tpaired_end")

mkdir -p $repository

cd $in_dir
while IFS='' read -r line || [[ -n "$line" ]]
         do
            #get dataset name
            d="$(find . | grep -i "$line" | head -1)"
            d=$(basename $d)
            echo "Processing folder $d"
            echo "---------------------------------------------------------------"

            # loop over FASTQ files in each dataset
            for fastq in $(find $d -name "*.fastq.gz")
                do
                  if [ -e $fastq ]
                      then
                          #get metadata file
                          json=$(find $(dirname "${fastq}") -name "metadata.json")
                        # echo "JSON file is: $json"
                          
                          run_type=$(cat $json | jq $run_type_tag )
                       #  echo "Run type for $fastq is $run_type"
                          if [ "$run_type" == "$PAIR_ENDED_LABEL" ]
                              then
                                  mkdir -p $repository/$d
                     		  #check if control file
                        	  if [[ "$(echo "$fastq" | tr '[:upper:]' '[:lower:]')" == *"control"* ]]
                                      then 
                                          file_type=$CONTROL_LABEL
                                          download_dir=$repository/$d/controls
                                       else
                                          file_type=$REPLICATE_LABEL
                                          download_dir=$repository/$d/replicates
                                  fi
                                  
                                  # get pair and order
                                  paired_with=$(cat $json | jq $paired_with_tag)
                                  paired_with=$(echo "$paired_with" | tr -d '"')
                                  paired_end=$(cat $json | jq $paired_end_tag)

                                  # create folder structure and download files
                                  mkdir -p $download_dir     
         			  wget $BASE_URL/files/$(basename ${fastq%.fastq.gz})/@@download/$(basename $fastq) -P $download_dir
				  wget $BASE_URL$paired_with@@download/$(basename $paired_with).fastq.gz -P $download_dir

#				  echo "Paired: $BASE_URL/files/$(basename ${fastq%.fastq.gz})/@@download/$(basename $fastq)"
#				  echo "With: $BASE_URL$paired_with@@download/$(basename $paired_with).fastq.gz"

                                  # rename so we keep track of the order in the file name
				  file_order="1"
                                  pair_order="2"
                                  if [ "$(echo "$paired_end" | tr -d '"')" == "2" ]
                                      then
                                         file_order="2"
                                         pair_order="1"
                                  fi
                                  mv $download_dir/$(basename $fastq) $download_dir/$(basename ${fastq%.fastq.gz})_${file_order}_pair_$(basename $paired_with).fastq.gz
                                  mv $download_dir/$(basename $paired_with).fastq.gz $download_dir/$(basename ${paired_with%.fastq.gz})_${pair_order}_pair_$(basename ${fastq%.fastq.gz}).fastq.gz

#         			  echo "File: $(basename ${fastq%.fastq.gz})_$file_order.fastq.gz"	 
#                                 echo "Pair: $(basename ${paired_with%.fastq.gz})_$pair_order.fastq.gz"
			      else
                                 #reset labels for single-ended
			         paired_with="NA"
 			         paired_end="NA"
                                 wget $BASE_URL/files/$(basename ${fastq%.fastq.gz})/@@download/$(basename $fastq) -P $download_dir
#                                 echo "Single: $BASE_URL/files/$(basename ${fastq%.fastq.gz})/@@download/$(basename $fastq)"
	                   fi
			
                     # add to summary table
                     RESULTS+=("$d\t$(basename $fastq)\t$file_type\t$run_type\t$paired_with\t$paired_end")
                  fi
             done
done < "$datasets"

#export table
printf '%b\n' "${RESULTS[@]}" > $curr_dir/ENCODE_dataset_details.tsv
