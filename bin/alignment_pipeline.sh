#!/bin/bash

# This script will execute the workflow needed to prepare the input data for the peak calling
# software (JAMM or MACS). It assumes that there is at least 1 data set in the
# input folder provided as the first argument and that the data set(s) contain(s) gzipped FASTQ files.
# More details about each step and the used parameters can be found in the diagram 
# "peak_calling_diagram.pdf" located in the doc folder.
#
# Dependencies: 
# -------------
# gzip
# bowtie2
# samtools
# bedtools2 
#i
# Parameters:
# -----------
# arg[1]: input folder containing the FASTQ data sets
# arg[2]: the bowtie2 index FASTA file. This file can be downloaded from
# ftp://ftp.ncbi.nlm.nih.gov/genomes/archive/old_genbank/Eukaryotes/vertebrates_mammals/Homo_sapiens/GRCh38/seqs_for_alignment_pipelines/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.bowtie_index.tar.gz
#
# The script has been adapted from
# https://github.com/mahmoudibrahim/JAMM/wiki/ChIP-Seq-Alignment-and-Processing-Pipeline
#
#
#
# Note: that for the pair-ended FASTQ files it assumes that the names of the files contain sufficient information for 
# retrieving the order of the files when calling the DNA sequence retrieval. For instance: ENCFF795KJT_1_ENCFF067HZB.fastq.gz# means that ENCFF795KJT is paired with ENCFF067HZB and it is the first one of the pair. The corresponding ENCFF067HZB_2_ENCFF795KJT.fastq.gz should be present in the same folder.
#
#
# usage: bash alignment_pipeline.sh  /path/to/fastq.gz/data/sets /path/to/bowtie2/index/file


###### !!! MODIFY ACCORDINGLY THE PATHS TO DEPENDENCIES ######
PATH_TO_BOWTIE2=
PATH_TO_SAMTOOLS=
PATH_TO_BEDTOOLS2=
##############################################################

#parse arguments
in_dir=;
if [ -z "$1" ]
    then
        echo "No input file folder specified. Exiting...";
        exit 1
    else
        in_dir=$1
fi

bowtie_index=;
if [ -z "$2" ]
    then
        echo "No BOWTIE 2 index file specified. Exiting...";
        exit 1
    else
        bowtie_index=$2;
fi

cd $in_dir

    in_dir=$(basename $in_dir)
    echo "Processing data set $in_dir";

    for fastq in $(find ./ -name "*.fastq.gz")
        do
            if [ -e $fastq ]
                then

               gunzip $fastq

               fastq=${fastq%.*}
               echo "$fastq"

               #check if pair-ended or not (example file name ENCFF795KJT_1_ENCFF067HZB.fastq.gz)
               fileIN=(${fastq//_/ })
               filename=${fileIN[0]} #get the file name
               order=${fileIN[1]} # get the order of a paired-ended FASTQ file
               paired_file=${fileIN[-1]%/*} #get last element which is the name of the paired file

               echo "STEP 1: ALIGNMENT USING BOWTIE2";
               #no paired file
               if [ "$paired_file" == "$(dirname $fastq)" ]
                   then
                        bowtie2 -x $bowtie_index -U $fastq -S $fastq.sam
#                        echo " bowtie2 -x $bowtie_index -U $fastq -S $fastq.sam"

                        rm $fastq
                   else
                      #figure out the order of the pair-ended files
                      paired_file_order=1
                      fastqIsFirst=false
                      if [ "$order" == "$paired_file_order" ]
                         then
                             paired_file_order=2
                             fastqIsFirst=true
                      fi
                      #build up the file name of the pair
                      paired_file=$(dirname $fastq)/${paired_file%.*}_${paired_file_order}_$(basename $filename).fastq.gz
                      #unzip and remove extension from variable
                      gunzip $paired_file
                      paired_file=${paired_file%.*}

                      echo "The paired file for $fastq is $paired_file"
                      # check if the paired file exists in the folder or exit with error
                      if [ -e ${paired_file} ]
                          then
                              out=$(dirname $fastq)/$(basename $filename)_${fileIN[-1]%/*}.sam
                              if $fastqIsFirst
                                  then
                                      bowtie2 -x $bowtie_index -1 $fastq -2 $paired_file -S $fastq.sam
#                                      echo "bowtie2 -x $bowtie_index -1 $fastq -2 $paired_file -S $out"
                                  else
                                      bowtie2 -x $bowtie_index -1 $paired_file -2 $fastq -S $fastq.sam
#                                      echo "bowtie2 -x $bowtie_index -1 $paired_file -2 $fastq -S $out"
                              fi
                          else
                             echo "Paired file for $fastq does not exist for data set $d"
                             exit 1
                      fi

                      #remove the fastq files
                      rm $fastq
                      rm $paired_file

               fi
               date  

		echo "STEP 2: FILTERING SAM";
		$PATH_TO_SAMTOOLS/samtools view -Sh $fastq.sam | \
		    grep -e "^@" -e "XM:i:[012][^0-9]" | \
		    grep -v "XS:i:" > $fastq.sam.filtered.sam
		date

		echo "STEP 3: SAM TO BAM CONVERSION"
		$PATH_TO_SAMTOOLS/samtools view -S -b $fastq.sam.filtered.sam \
		    > $fastq.sam.filtered.sam.bam
		$PATH_TO_SAMTOOLS/samtools sort -o $fastq.sam.filtered.sam.bam.sorted.bam \
		    $fastq.sam.filtered.sam.bam
		date;

		echo "STEP 4: REMOVE PCR DUPLICATES";
		$PATH_TO_SAMTOOLS/samtools rmdup -s $fastq.sam.filtered.sam.bam.sorted.bam \
		    $fastq.sam.filtered.sam.bam.sorted.bam.nodup.bam
		date

		echo "STEP 5: BAM INDEXING";
		$PATH_TO_SAMTOOLS/samtools index $fastq.sam.filtered.sam.bam.sorted.bam.nodup.bam
		date
	
		echo "STEP 6: BAM TO BED CONVERSION";
		$PATH_TO_BEDTOOLS2/bedtools bamtobed \
		    -i $fastq.sam.filtered.sam.bam.sorted.bam.nodup.bam > \
		    $fastq.sam.filtered.sam.bam.sorted.bam.nodup.bam.bed
		date

		echo "STEP 7: GZIPPING FILES";
		gzip $fastq.sam.filtered.sam.bam.sorted.bam.nodup.bam
		gzip $fastq.sam.filtered.sam.bam.sorted.bam.nodup.bam.bai
		gzip $fastq.sam.filtered.sam.bam.sorted.bam.nodup.bam.bed
		date

	    fi	
done

